<!-- **************************** -->
<!-- ***** TOP HEADER NAVBAR **** -->
<!-- **************************** -->
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
            <a class="navbar-brand" href="#">
        <img src="./img/logo.png">
      </a>

    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<ul class="nav navbar-nav">
		    <li class="active">
		    	<a href="#">Activity Feed</a>
			</li>
		    <li>
		    	<a href="#">(Takeshi Kimura)</a>
		    </li>
		    <li>
		    	<a href="#">Query</a>
			</li>
			<li>
				<a href="#">Message</a>
			</li>
			<li>
				<a href="#">Client Communication</a>
			</li>
		</ul>


      <ul class="nav navbar-nav navbar-right">
        <li>
        	<a href="#">Icon1</a>
    	</li>
        <li>
        	<a href="#">Icon2</a>
    	</li>
        <li>
        	<a href="#">Icon3</a>
    	</li>
        <li>
        	<a href="#">Icon4</a>
    	</li>
        <li>
        	<a href="#">Icon5</a>
    	</li>

      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<!-- ******************************** -->
<!-- ***** ENDTOP HEADER NAVBAR **** -->
<!-- ******************************** -->