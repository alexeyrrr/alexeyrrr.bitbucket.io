<!DOCTYPE html>
<html lang="en"><head>
<meta charset="UTF-8">
<title>JURISTERRA</title>
<meta name="application-name" content="JURISTERRA">
<meta name="msapplication-TileColor" content="#0E2B5E">
<meta name="description" content="">
<meta name="keywords" content="">
<!-- stylesheet -->
<link href="http://fonts.googleapis.com/css?family=Lato&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="css/common/normalize.css">
			<link rel="stylesheet" href="css/common/style.css">
		<link rel="stylesheet" href="css/plugin/select2.min.css">
	<link rel="stylesheet" href="css/common/invite.css">
	<link rel="stylesheet" href="css/common/notification.css">
<link rel="stylesheet" href="css/juristerra.css">

<!-- cache -->
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
<meta http-equiv="Expires" content="0">
<!-- viewport -->
<script async="" src="https://www.google-analytics.com/analytics.js"></script><script src="/build/react.min.js"></script>
<script src="/build/react-dom.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.24/browser.min.js"></script>
<script src="https://code.jquery.com/jquery-2.2.2.min.js" integrity="sha256-36cp2Co+/62rEAAYHLmRCPIych47CvdM+uTBJwSzWjI=" crossorigin="anonymous"></script>
<script type="text/javascript" src="/js/juristerra.js"></script><script type="text/javascript" src="/js/jquery-ui.min.js"></script><script type="text/javascript" src="/js/jquery.bottom-1.0.js"></script>
	<style type="text/css">
		.select2-hidden-accessible { height:0px; display:none; }
		.select-input-box select{ height:39px; overflow:hidden; background-color:#FFFFFF; }
		.select2-results__option{ padding-left:5px; width:250px; }
		.post-setting_baloon a{display: block; width:100% !important; }
		.post-setting_baloon br{display: none;}
		.post-setting_baloon a.head-meta_first { margin-bottom:0px !important; }
	</style>
<script type="text/javascript"> $(function() { $(".select-input-box").css('height', 'auto'); }); </script>
<meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, maximum-scale=1.0, user-scalable=yes">

<!-- analitics ??????? -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-79529691-1', 'auto');
  ga('send', 'pageview');
</script>
<!-- analitics ??????? end -->
</head>

	<body>
		<header id="header" class="clear" style="left: 0px;">
		<div class="innerBox">
	<a href="/mypage" id="logo">
		<img src="/img/common/logo.png" width="100%">
	</a>
	<ul class="top-menu-text">
	<li>
	ACTIVITY FEED
	</li>
	<li>
	USERNAME
	</li>
	<li>
	QUERY
	</li>
	<li>
	<a href="message.php">
	MESSAGE
	</a>
	</li>
	<li>
	CLIENT COMMUNICATION
	</li>
	</ul>
	<ul id="nav" class="clear">
			<li class="btn_block">
				<a rel="searchModal" href="#modal_search" class="menu_tag">
					<img src="/img/common/icon_head01.png" alt="Search" width="100%">
					<span class="menu_test">Search</span>
				</a>
			</li>
			<li class="btn_block">
				<a rel="searchModal" href="#modal_search" class="menu_tag">
					<img src="/img/common/iconsetting-50px.png" alt="Settings" width="100%">
					<span class="menu_test">Settings</span>
				</a>
			</li>
			<li class="btn_block">
				<span class="menu_up menu_tag">
					<img src="/img/common/icon_head04.png" alt="Notification" width="100%">
					<span class="menu_test">Notification</span>
																													<div class="notice_area">
						<div class="notice_fix">
							<div class="notice_head">
								<h3 class="notice_title">Notification</h3>
								<div class="noteice_setting">
									<a id="readall" href="/mypage/notification_read_all/">Mark All as Read</a> / <a href="/users/notifications">Settings</a>
								</div>
							</div><!--/notice_head-->
							<div class="clear"></div>
							<div class="notice_content">
								<ul class="notice_list">
																																</ul><!--/notice_list-->
							</div><!--/notice_content-->
						</div><!--/notice_fix-->
						<div class="more_btn">
							<a href="/mypage/notification_all">VIEW ALL</a>
						</div>
					</div><!--/notice_area-->
				</span>
			</li>
										<li class="btn_block">
					<a rel="leanModal" href="#modal_inv" class="invite menu_tag" id="modalInvOpen">
						<img src="/img/common/icon_head05.png" alt="Invite" width="100%">
						<span class="menu_test">Invite</span>
					</a>
				</li>
	</ul>
</div>		</header>

<script type="text/javascript">
	$("#readall").click(function() { $.ajax({url: $(this).attr('href') }).done(function(data) {
		$(".exnotif, .exnotif2").remove();
		$(".notice_area").parent().find('.icon_green').remove();
	}); return false; });
	$(".exnotif a").click(function() {
		arg = $(this).attr('id').split(':');
		notif_elem = $(this).parent();
		ncount_elem = notif_elem.closest(".btn_block").find(".icon_green");
		$.ajax({url: '/mypage/notification_read/' + arg[1] + '/' }).done(function(data) {
			if(data == 1) notif_elem.animate({ opacity: 0, width: '1px' }, 500, function() {
				notif_elem.slideUp(300, function() {
					 ncount = ncount_elem.html() - 1;
					 if(ncount &gt; 0) ncount_elem.html(ncount);
					 else ncount_elem.remove();
				});
			});
		});
		return false;
	});

	// global
	var refreshUnreadMessageCount;
	$(function () {
		refreshUnreadMessageCount = function () {
			$.get('/communications/message/unread_message_count', function (res) {
				if (res) {
					var $title = $('title');
					var all_count = res.unread_message_count
												+ res.unread_query_attorney_count
												+ res.unread_query_client_count
												+ res.unread_query_myquery_count;
					if (all_count) {
						$('.communication-unread-count').text(all_count).show();
						$title.html('(' + all_count + ')' + $title.html().replace(/^\(\d+\)/, ''));
					} else {
						$('.communication-unread-count').empty().hide();
						$title.html($title.html().replace(/^\(\d+\)/, ''));
					}

					if (res.unread_message_count) {
						$('.communication-unread-message-count').text(res.unread_message_count).show();
					} else {
						$('.communication-unread-message-count').empty().hide();
					}

					if (res.unread_query_attorney_count) {
						$('.communication-unread-query-attorney-count').text(res.unread_query_attorney_count).show();
					} else {
						$('.communication-unread-query-attorney-count').empty().hide();
					}

					if (res.unread_query_client_count) {
						$('.communication-unread-query-client-count').text(res.unread_query_client_count).show();
					} else {
						$('.communication-unread-query-client-count').empty().hide();
					}

					if (res.unread_query_myquery_count) {
						$('.communication-unread-query-myquery-count').text(res.unread_query_myquery_count).show();
					} else {
						$('.communication-unread-query-myquery-count').empty().hide();
					}

					var unread_received_query_count = res.unread_query_attorney_count + res.unread_query_client_count;
					if (unread_received_query_count) {
						$('.communication-unread-received-query-count').text(unread_received_query_count).show();
					} else {
						$('.communication-unread-received-query-count').empty().hide();
					}
				}
			});
		};
				setInterval(refreshUnreadMessageCount, 60000);
		refreshUnreadMessageCount();
			});
</script>
	

<div id="fb-root"></div>


<div id="wrapper">
    <!-- [ / column-main ] -->

            <!-- [ / column-sub] -->
        
        <aside style="left: 0px;">
		<div>
		<ul class="message-side-tabs">
		<li>
		<a>
		Inbox
		</a>
		</li>
		<li class="message-side-tabs-current">
		<a>
		Outbox
		</a>
		</li>
		<li>
		<a>
		Search
		</a>
		</li>
		</ul>
		</div>
        </aside>
    </div>

<div id="modal_postEdit"></div>
<div id="modal_share" class="modal_win">
</div>
<!--#include virtual="/include/foot.html"-->
<script type="text/javascript">
function hidePost() {
    $(".hidePost").click(function() {
        post_box = $(this).closest(".post-box")
        $.ajax({ url:$(this).attr('href') }).done(function(data) {
            if(data == 'hidden') post_box.animate({ opacity: 0 }, 500, function() { post_box.slideUp(200); });
        });
        return false;
    });
	$(".delPost").click(function() {
        post_box = $(this).closest(".post-box")
        $.ajax({ url:$(this).attr('href') }).done(function(data) {
            if(data == 'deleted') post_box.animate({ opacity: 0 }, 500, function() { post_box.slideUp(200); });
        });
        return false;
    });
    }
hidePost();
</script>
<script src="/js/plugin/jquery.slimscroll.min.js"></script>
<script>
    $(function(){
        $( 'a[rel*=leanPost]').leanModal({
            top: 0,                     // ????????????????
            overlay : 0.9,               // ??????
            closeButton: ".modal_close"  // ???????CSS class???
        });
        $( 'a[rel*=leanModal]').leanModal({
            top: 0,                     // ????????????????
            overlay : 0.9,               // ??????
            closeButton: ".modal_close"  // ???????CSS class???
        });
        $("a[rel*=leansave]").leanModal({
            top: 60,
            overlay : 0.9,
            closeButton: ".modal_close"
        });
        var token = '098010f2b9655cc42010d9d6a7c7c99806d75d34';
        $(window).bottom({proximity: 0.00001}); //proximity?0.5????????50%??????????loading?????
        $(window).bind("bottom", function() {
            var string = $('.loading_posts').val();
            $('.loading_posts').remove();
            var end_flag = $('.end_flag').val(); //????????????1?????????????
            if(end_flag==0){ //???????????????
                var obj = $(this);
                if (!obj.data("#icon_loading")) {
                    obj.data("#icon_loading", true);
                    $('#icon_loading').append('&lt;img src="/img/common/icon_loading.png" alt="????"&gt;'); //???loading...???
                    setTimeout(function() {
                        string = Number(string) + 35;
                        var getId = $("#timeline-tab li[class='selected']").attr("id");
                        if (getId) {
                            var getUrl = getId.substr(4);
                        }else {
                            var getUrl = '';
                        }
                        var action = 'mypage';
                        var targetId = '';
                        var sortkey = ($('#sssort').attr('sortkey') != "" &amp;&amp; $('#sssort').attr('sortkey') != "null") ? $('#sssort').attr('sortkey') : '';
                        if (sortkey != "") {
                            sortkey = sortkey.split(",");
                        }
                        var sortby = ($('#sssort').attr('sortby') != "" &amp;&amp; $('#sssort').attr('sortby') != "null") ? $('#sssort').attr('sortby') : '';
                        if (sortby) {
                            sortby = sortby.split(",");
                        }

                        $.ajax({
                            type: "POST",
                            url: "/mypage/timeline",
                            dataType:"json",
                            data: {'loading':string, 'getUrl':getUrl, 'sortby':sortby, 'sortkey':sortkey, 'action':action, 'targetId':targetId, 'token':token},
                            success: function(data){
                                for (var Mo in data[0]) {break;}
                                var json = parseJsonSelect(data);
                                $("#post-wrap").append(json);
                                $('.data').empty();
                                $('.data').append('&lt;input class="loading_posts" type="hidden" value=\'' +data[1]+ '\'&gt;');
                                $('.data').append('&lt;input class="end_flag" type="hidden" value=\'' +data[2]+ '\'&gt;');
                                $('.data').append('&lt;div id="sssort" sortkey="' +data[4]+ '" sortby="' +data[3]+ '"&gt;&lt;/div&gt;');
                                hidePost();
                            },
                            error: function(XMLHttpRequest, textStatus, errorThrown){
                                console.log(XMLHttpRequest.status);
                            }
                        });
                    $("#icon_loading").empty(); //???????loading...?????
                        obj.data("#icon_loading", false);
                    }, 1000);
                }
            } //end_flag
        });

        var emptyData;
        var postWrap = $("#post-wrap");
        var postItem = $("#timeline-tab li");
        var conWrap = $("#con-wrap");
        var conItem = $("#contact-tab li");

        var sortkey;
        var sortby;

        var action = 'mypage';
        var targetId = '';

        $.ajax({
            type: "POST",
            url: "/mypage/timeline",
            dataType:"json",
            data: {'getUrl':'all', 'sortby':sortby, 'sortkey':sortkey, 'action':action, 'targetId':targetId, 'token':token},
            success: function(data){
                for (var Mo in data[0]) {break;}
                var json = parseJsonSelect(data);
                $("#post-wrap").append(json);
                $('.data').append('&lt;input class="loading_posts" type="hidden" value=\'' +data[1]+ '\'&gt;');
                $('.data').append('&lt;input class="end_flag" type="hidden" value=\'' +data[2]+ '\'&gt;');
                $('.data').append('&lt;div id="sssort" sortkey="' +data[4]+ '" sortby="' +data[3]+ '"&gt;&lt;/div&gt;');
                hidePost();
            },
            error: function(XMLHttpRequest, textStatus, errorThrown){
                console.log(XMLHttpRequest.status);
            }
        });

        $(document).on('click',".tab li:not(.selected)",function(){
            var getId = $(this).attr("id");
            getId = getId.substr(4); // "tab-"???
            var getUrl = "/ajax/"+getId+".html";
            var ajaxField = $(this).parent().attr("id");
            if(ajaxField == "timeline-tab"){ // ????????
                emptyData = postWrap;
                tabClass = postItem;
            }else if(ajaxField == "contact-tab"){ // ???????
                emptyData = conWrap;
                tabClass = conItem;
            }

            //??????????????
            if(emptyData != "" &amp;&amp; emptyData != conWrap){
                tabClass.removeClass("selected");
                $(this).addClass("selected");
                emptyData.empty();
                $.ajax({
                    type: "POST",
                    url: "/mypage/timeline",
                    dataType:"json",
                    data: {'getUrl':getId, 'token':token},
                    success: function(data){
                        var json = parseJsonSelect(data);
                        emptyData.append(json);
                        $('.data').empty();
                        $('.data').append('&lt;input class="loading_posts" type="hidden" value=\'' +data[1]+ '\'&gt;');
                        $('.data').append('&lt;input class="end_flag" type="hidden" value=\'' +data[2]+ '\'&gt;');
                        $('.data').append('&lt;div id="sssort" sortkey="' +data[4]+ '" sortby="' +data[3]+ '"&gt;&lt;/div&gt;');
                        hidePost();
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown){
                        console.log(XMLHttpRequest.status);
                    }
                });
            }else if(emptyData != "" &amp;&amp; emptyData == conWrap ) {
                tabClass.removeClass("selected");
                $(this).addClass("selected");
                emptyData.empty();
                $.ajax({
                    type: "GET",
                    url: "/mypage/recent_messages",
                    dataType:"json",
                    data:{'getId': getId},
                    success: function(data,dataType){
                        if (data.html) {
                            emptyData.html(data.html);
                        }
                        if ('unread_message_count' in data) {
                            $.each(['message', 'query', 'myquery'], function (i, v) {
                              var $icon = $('#tab-' + v).children('.icon_green');
                              $icon.hide();
                              if (data['unread_' + v + '_count'] &gt; 0) {
                                  $icon.text(data['unread_' + v + '_count']).show();
                              }
                            });
                        }
                        hidePost();
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown){}
                });
            }
        });

        conItem.eq(0).trigger('click');
        // ?????????????
        $('#column-sub').find('.btn_more').on('click', function (e) {
            e.preventDefault();
            var selectedId = $("#contact-tab li.selected").attr('id');
            if (selectedId == 'tab-message') {
                location.href = '/communications';
            }
            else if (selectedId == 'tab-query') {
                location.href = '/communications/query';
            }
            else if (selectedId == 'tab-myquery') {
                location.href = '/communications/query/myquery';
            }
        });

        // ??????
        var range = $( '#btn_range input[name="range"]' );
        var range0 = $('#range_0');
        var range1 = $('#range_1');
        var range10 = $('#range_10');
        var rangeX = $('#btn_range input[type=checkbox]');
        var pushBox = $('#btn_range ul');

        range.change( function() {
            if($( this ).val() == "Limit who can see this post"){
                range10.prop('checked',true);
                pushBox.html('&lt;li class="tag-select"&gt;Limit who can see this post&lt;/li&gt;');
            }else{
                rangeX.prop('checked',false);
                pushBox.html('&lt;li class="tag-select"&gt;All Members&lt;/li&gt;');
            }
        });

        rangeX.change( function(){
            if($('#btn_range input[type=checkbox]:checked').length == 0){
                range0.prop('checked',true);
                pushBox.html('&lt;li class="tag-select"&gt;All Members&lt;/li&gt;');
            }else{
                range1.prop('checked',true);
                pushBox.html('&lt;li class="tag-select"&gt;Limit who can see this post&lt;/li&gt;');
            }
        });

        // ????????OPEN,CLOSE
        var btnRange = $('#btn_range');
        var chkBox = $('#btn_range .chk-box');
        btnRange.click(function(evt){
            if(!btnRange.hasClass('open')){
                btnRange.addClass('open');

            }else if(!$(evt.target).closest(chkBox).length){
                btnRange.removeClass('open');
            }
        });

        $(document).on('click', function(evt){
            if( !$(evt.target).closest(btnRange).length ){
                btnRange.removeClass('open');
            }
        });

        //???????????????
        $('.sort_submit').click(function(){
            var sortkey = $('#sortby').val();
            var sortby = [];
            $('#timeline').find('li[class="select2-selection__choice"]').each(function(index, elem){
                sortby.push($(elem).attr('title'));
            });
            var getId = $("#timeline-tab li[class=selected]").attr("id");
            var getUrl = getId.substr(4);

            $("#post-wrap").empty();
            $.ajax({
                type: "POST",
                url: "/mypage/timeline",
                dataType:"json",
                data: {'sortkey':sortkey, 'sortby':sortby, 'getUrl':getUrl, 'token':token},
                success: function(data){
                    var json = parseJsonSelect(data);
                    $("#post-wrap").append(json);
                    $('.data').empty();
                    $('.data').append('&lt;input class="loading_posts" type="hidden" value=\'' +data[1]+ '\'&gt;');
                    $('.data').append('&lt;input class="end_flag" type="hidden" value=\'' +data[2]+ '\'&gt;');
                    $('.data').append('&lt;div id="sssort" sortkey="' +data[4]+ '" sortby="' +data[3]+ '"&gt;&lt;/div&gt;');
					hidePost();
                },
                error: function(XMLHttpRequest, textStatus, errorThrown){
                    console.log(XMLHttpRequest.status);
                }
            });

        });

        //????????????
        $("#post-wrap").on('click', '.postTAG', function(){
            var sortkey = [];
            var sortby = [];
            sortkey.push($(this).attr('ky'));
            sortby.push($(this).html());
/*            $('#timeline').find('li[class="select2-selection__choice"]').each(function(index, elem){
                sortby.push($(elem).attr('title'));
            });
*/          var getId = $("#timeline-tab li[class=selected]").attr("id");
            if (getId) {
                var getUrl = getId.substr(4);
            }else {
                var getUrl = '';
            }
            var action = 'mypage';
            var targetId = '';

            $("#post-wrap").empty();
            $.ajax({
                type: "POST",
                url: "/mypage/timeline",
                dataType:"json",
                data: {'sortkey':sortkey, 'sortby':sortby, 'getUrl':getUrl, 'action':action, 'targetId':targetId, 'token':token},
                success: function(data){
                    var json = parseJsonSelect(data);
                    $("#post-wrap").append(json);
                    $('.data').empty();
                    $('.data').append('&lt;input class="loading_posts" type="hidden" value=\'' +data[1]+ '\'&gt;');
                    $('.data').append('&lt;input class="end_flag" type="hidden" value=\'' +data[2]+ '\'&gt;');
                    $('.data').append('&lt;div id="sssort" sortkey="' +data[4]+ '" sortby="' +data[3]+ '"&gt;&lt;/div&gt;');
					hidePost();
                },
                error: function(XMLHttpRequest, textStatus, errorThrown){
                    console.log(XMLHttpRequest.status);
                }
            });

        });

        //??????????????????
        $('#search-form').on('input', searchConnection);

        function searchConnection(e) {
            var page = 1;
            var searchWord = document.querySelectorAll('input[class="txt_12"]');
            var q = searchWord[0].value;
            ajaxSearchConnection(q, page);
        }

        function ajaxSearchConnection(q, page){
            $.post("/invite_requests/userlist/", {searchWord:q, page:page, selection:'connect', userId:'2'}, function(data){
                if (data.length&gt;0){
                    var json = $.parseJSON(data);
                    var data = parseJsonConnection(json);
                    $('#connection-box').empty();
                    $('#connection-box').append(data);
                }
            });
        }

        function parseJsonConnection(json) {
            var num = json.length;
            var ret = '';
            for (i=0; i &lt; num; i++) {
                if (json[i].User.profile_image1_dir == null) {
                    json[i].User.profile_image1_dir = '';
                }

                ret +=  '&lt;a href="/communications/message/new?uid=' +json[i].User.id+ '" class="contact-item clear icon_online"&gt;';
                ret +=  '&lt;div class="icon_circle"&gt;';
                if (json[i].User.profile_image1) {
                    ret +=  '&lt;img src="/images' +json[i].User.profile_image1_dir+ '/' +json[i].User.profile_image1+ '" alt=""&gt;';
                }else{
                    ret +=  '&lt;img src="/images/noimage.jpg" alt=""&gt;';
                }
                ret +=  '&lt;/div&gt;';
                ret +=  '&lt;div class="connection-cap"&gt;';
                ret +=  '&lt;p class="txt_12 contact-name"&gt;' +json[i].User.username+ '&lt;/p&gt;' ;
                if (json[i].Office.name != "" &amp;&amp; json[i].Office.name != null) {
                    ret +=  '&lt;p class="txt_12"&gt;' +json[i].Office.name+ '&lt;/p&gt;';
                }else{
                    ret +=  '&lt;p class="txt_12"&gt;----&lt;/p&gt;';
                }
                ret +=  '&lt;/div&gt;';
                ret +=  '&lt;/a&gt;';
            }
            return ret;
        }

        // ???????????

        var postEdit = $('#modal_postEdit'),
        body = $('body'),
        modalShare = $('#modal_share');
        var editedPost;
        $(document).on('click','a[rel*=leanPost]', function(){
            var postId = $(this).attr("pid");
            var kind = $(this).attr("kind");
            postId = postId.substr(9);
            editedPost = $(this);
            $.ajax({
                type: "POST",
                url: "/posts/ajaxEditPost",
                dataType:"json",
                data:{"postId":postId, 'token':token, 'kind':kind},
                success: function(data,dataType){
                    var dta = editModal(data);
                    postEdit.html(dta);
                    body.addClass("win_02");
                },
                error: function(XMLHttpRequest, textStatus, errorThrown){}
            });
            return false;
        }).on('click',function(evt){
            if( !$(evt.target).closest('#modal_postEdit').length
                &amp;&amp; !$(evt.target).closest('a[rel*=leanPost]').length
                &amp;&amp; !$(evt.target).hasClass('select2-selection__choice__remove')
            ){
                body.removeClass('win_02');
            }
        });

        function editModal (data) {
            var juri =[];
            $.each(data.Admission, function(ky, val){
                juri.push(val);
            });
            var law =[];
            $.each(data.SpecialField, function(ky, val){
                law.push(val.SpecialField.law_field_prefix + val.SpecialField.law_field_id);
            });
            var tag =[];
            $.each(data.MyTag, function(ky, val){
                tag.push(ky);
            });
            var ret = '';
            ret += '&lt;p class="txt_14 fw_b" id="display_err" style="background-color:#fff;"&gt;&lt;/p&gt;';
            ret += '&lt;div&gt;&lt;form class="post-form" method="post" action="/posts/edit_comp" enctype="multipart/form-data" name="edForm"&gt;';
            ret += '&lt;div class="clear"&gt;';
            ret += '&lt;div class="select-input-box"&gt;';
            ret += '&lt;select name="data[admission][]" id="jurisdiction_sub" multiple="multiple"&gt;';
            $.each(data.jurisdiction_list, function(index, elem) {
                ret += '&lt;option' + ((juri.indexOf(index) != -1) ? ' selected' : '') + ' value=' +index+ '&gt;' +elem+ '&lt;/option&gt;';
            });
            ret += '&lt;/select&gt;';
            ret += '&lt;span class="btn_arrow"&gt;&lt;/span&gt;';
            ret += '&lt;/div&gt;';
            ret += '&lt;div class="select-input-box"&gt;';
            ret += '&lt;select name="data[special_field][]" id="lawField_sub" multiple="multiple"&gt;';
            $.each(data.law_field_list, function(index, elem) {
                ret += '&lt;option' + ((law.indexOf(index) != -1) ? ' selected' : '') + ' value=' +index+ '&gt;' +elem+ '&lt;/option&gt;';
            });
            ret += '&lt;/select&gt;';
            ret += '&lt;span class="btn_arrow"&gt;&lt;/span&gt;';
            ret += '&lt;/div&gt;&lt;/div&gt;';
            ret += '&lt;div class="post-txt"&gt;';
            ret += '&lt;div class="spacer-scrollY"&gt;';
            ret += '&lt;textarea name="editComment" placeholder="Text" style="background-color:#fff;"&gt;' +data.Comment+ '&lt;/textarea&gt;';
            ret += '&lt;/div&gt;&lt;/div&gt;';
            ret += '&lt;div class="bg_tag select-input-box"&gt;';
            ret += '&lt;select name="data[post_tag][]" id="postTags_sub" multiple="multiple"&gt;';
            $.each(data.Tag, function(index, elem) {
                ret += '&lt;option' + ((tag.indexOf(elem) != -1) ? ' selected' : '') + ' value="' +elem+ '" ky="' + index + '"&gt;' +elem+ '&lt;/option&gt;';
            });
            ret += '&lt;/select&gt;';
            ret += '&lt;span class="btn_arrow"&gt;&lt;/span&gt;';
            ret += '&lt;/div&gt;';
            ret += '&lt;div class="clear"&gt;';
            ret += '&lt;label class="btn_form txt_gray"&gt;';
            ret += 'Add Photos';
            ret += '&lt;input type="file" name="data[img]" multiple="multiple" id="img_sub"&gt;';
            ret += '&lt;/label&gt;';
            ret += '&lt;div class="btn_range select-input-box txt_11"&gt;';
            ret += '&lt;span class="btn_arrow"&gt;&lt;/span&gt;';
            ret += '&lt;span class="btn_select txt_gray txt_11"&gt;';
            ret += '&lt;ul class="clear"&gt;';
            ret += '&lt;li class="tag-select"&gt;' +data.range+ '&lt;/li&gt;';
            ret += '&lt;li class="txt_range"&gt;&lt;div&gt;Who can see this post?&lt;/div&gt;&lt;/li&gt;';
            ret += '&lt;/ul&gt;&lt;/span&gt;';
            ret += '&lt;div class="chk-box" id="range" style="background-color:#fff;"&gt;';
            ret += '&lt;input type="radio" value="All Members" name="range" id="r_0" class="input_radio" ' +data.rangeCheck1+ '&gt;&lt;label for="r_0"&gt;All Members&lt;/label&gt;';
            ret += '&lt;input name="range" value="Limit who can see this post" type="radio" id="r_1" class="input_radio" ' +data.rangeCheck2+ '&gt;&lt;label for="r_1"&gt;Limit who can see this post&lt;/label&gt;';
            ret += '&lt;div class="chk-box"&gt;';
            ret += '&lt;input type="checkbox" value="All Attorneys" name="data[range_det][]" id="r_10" class="input_checkbox" ' +data.rangeCheck3+ '&gt;&lt;label for="r_10"&gt;All Attorneys&lt;/label&gt;';
            ret += '&lt;input type="checkbox" value="All Clients" name="data[range_det][]" id="r_11" class="input_checkbox" ' +data.rangeCheck4+ '&gt;&lt;label for="r_11"&gt;All Clients&lt;/label&gt;';
            ret += '&lt;input type="checkbox" value="Attorneys Connected" name="data[range_det][]" id="r_12" class="input_checkbox" ' +data.rangeCheck5+ '&gt;&lt;label for="r_12"&gt;Attorneys Connected&lt;/label&gt;';
            ret += '&lt;input type="checkbox" value="Client Connected" name="data[range_det][]" id="r_13" class="input_checkbox" ' +data.rangeCheck6+ '&gt;&lt;label for="r_13"&gt;Client Connected&lt;/label&gt;';
            ret += '&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;';
            ret += '&lt;input type="hidden" name="post_id" value="' +data.postId+ '"&gt;';
            ret += '&lt;input type="hidden" name="token" value="' +data.token+ '"&gt;';
            ret += '&lt;input type="hidden" name="kind" value="' +data.kind+ '"&gt;';
            ret += '&lt;input id="hoge" type="hidden" value="hoge"&gt;';
            ret += '&lt;div class="btn_form bg_green" id="postSubmit"&gt;';
            ret += 'Post';

            ret += '&lt;/div&gt;&lt;/div&gt;&lt;/form&gt;';
            ret += '&lt;span class="modal_close"&gt;Cancel&lt;/span&gt;';
            ret += ''
             +'&lt;script&gt;'
             + '$(function(){'
             +    '$("#jurisdiction_sub").select2({'
             +      'placeholder: "Add Jurisdiction"'
             +    '});'
             +    '$("#lawField_sub").select2({'
             +      'placeholder: "Add Area of Law"'
             +    '});'
             +    '$("#postTags_sub").select2({'
             +      'placeholder: "Add Tags",'
             +      'tags: true'
             +    '});'
             +       '$("#sortby_sub").select2({'
             +             'placeholder: "SORT BY"'
             +           '});'

             +           "$('#modal_postEdit .select-input-box li:first-child input').css({'width':'229px','minHeight':'38px'});"
             +           "$('#modal_postEdit .select-input-box .select2.select2-container.select2-container--default').css({'width':'229px','minHeight':'38px'});"
             +           "$('#modal_postEdit .bg_tag li:first-child input').css({'width':'437px','minHeight':'38px'});"
             +           "$('#modal_postEdit .bg_tag .select2.select2-container.select2-container--default').css({'width':'437px','minHeight':'38px'});"

             +       "var r = $( '#modal_postEdit .btn_range input[name=\"range\"]' );"
             +       "var r0 = $('#r_0');"
             +       "var r1 = $('#r_1');"
             +       "var r10 = $('#r_10');"
             +       "var rX = $('#modal_postEdit .btn_range input[type=checkbox]');"
             +       "var pBox = $('#modal_postEdit .btn_range ul');"

             +    'var range = $( \'#btn_range input[name="range"]\' );'
             +    "var range0 = $('#range_0');"
             +    "var range1 = $('#range_1');"
             +    "var range10 = $('#range_10');"
             +    "var rangeX = $('#btn_range input[type=checkbox]');"
             +    "var pushBox = $('#btn_range ul');"

             +           "r.change( function() {"
             +               'if($( this ).val() == "Limit who can see this post"){'
             +                   "r10.prop('checked',true);"
             +                   "pBox.html('&lt;li class=\"tag-select\"&gt;Limit who can see this post&lt;/li&gt;');"
             +               "}else{"
             +                   "rX.prop('checked',false);"
             +                   "pBox.html('&lt;li class=\"tag-select\"&gt;All Members&lt;/li&gt;');"
             +               "}"
             +           "});"

             +           "rX.change( function(){"
             +               "if($('#modal_postEdit .btn_range input[type=checkbox]:checked').length == 0){"
             +                   "r0.prop('checked',true);"
             +                   "pBox.html('&lt;li class=\"tag-select\"&gt;All Members&lt;/li&gt;');"
             +               "}else{"
             +                   "r1.prop('checked',true);"
             +                   "pBox.html('&lt;li class=\"tag-select\"&gt;Limit who can see this post&lt;/li&gt;');"
             +               "}"
             +           "});"

             +           "$('#modal_postEdit .btn_range').click(function(evt){"
             +               "if(!$('#modal_postEdit .btn_range').hasClass('open')){"
             +                   "$('#modal_postEdit .btn_range').addClass('open');"
             +               "}else if(!$(evt.target).closest($('#modal_postEdit .btn_range .chk-box')).length){"
             +                   "$('#modal_postEdit .btn_range').removeClass('open');"
             +               "}"
             +           "});"

             +           "$(document).on('click', function(evt){"
             +               "if( !$(evt.target).closest($('#modal_postEdit .btn_range')).length ){"
             +                   "$('#modal_postEdit .btn_range').removeClass('open');"
             +               "}"
             +           "});"

             +       "});"
             +  '&lt;\/script&gt;'
             + '&lt;/div&gt;&lt;/div&gt;';
            return ret;
        }

        //???????????????
        var postEdit = $('#modal_postEdit');
        var body = $('#modal_postEdit,body');
        var pushFlag = 0;
        $('#modal_postEdit').on('click', '#postSubmit',function(){
            if (pushFlag) return false;
            var comment = $('textarea[name="editComment"]').val();
            var fd = new FormData($('form[name="edForm"]').get(0));
            $.ajax({
                type: "POST",
                url: "/posts/ajaxEditPostComp",
                data:fd,
                //dataType:"json",
                processData: false,
                contentType: false,
                success: function(data, dataType){
				    if ( data == '4' ){
                        // ???
                        var str = '&lt;p style="color:#FF0000;"&gt;Please complete this field.&lt;/p&gt;';
                        $("#display_err").html(str);

                    } else {
                        updatePostHtml($('form[name="edForm"]'), data);
                        $("a[rel*=edit_complete]").leanModal({
                            top: 60,
                            overlay : 0.9,
                            closeButton: ".modal_close"
                        }).trigger('click');
                        postEdit.html('');
                        body.removeClass("win_02");
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown){}
            });
            pushFlag = 1;
            setTimeout(function(){ pushFlag = 0;}, 3000);
            return false;
        });

    	function updatePostHtml(fd, imgdata) {
            var args = imgdata.split('::');
            upimg = ''; if(imgdata != '1') upimg = args[0];

            var postBlock = editedPost.closest(".post-box");
            if(imgdata == '3') {
                postBlock.find(".post-content").eq(0).find("p").html(fd.find("textarea[name='editComment']").val().replace(/\r?\n|\r/g, "&lt;br /&gt;"));
            } else {
                var editedTags = '';
                fd.find("select[name='data[admission][]'] option:selected").each(function() {
                    editedTags += '&lt;a href="" onclick="return false;" class="postTAG" ky="' + $(this).val() + '"&gt;' + $(this).html() + '&lt;/a&gt; ';
                });
                fd.find("select[name='data[special_field][]'] option:selected").each(function() {
                    editedTags += '&lt;a href="" onclick="return false;" class="postTAG" ky="' + $(this).val() + '"&gt;' + $(this).html() + '&lt;/a&gt; ';
                });
				fd.find("select[name='data[post_tag][]'] option:selected").each(function() {
                    editedTags += '&lt;a href="" onclick="return false;" class="postTAG" ky="tagName' + $(this).attr('ky') + '"&gt;' + $(this).html() + '&lt;/a&gt; ';
                });
                postBlock.find(".txt_tags").html(editedTags);
                postBlock.find(".post-content p").html(fd.find("textarea[name='editComment']").val().replace(/\r?\n|\r/g, "&lt;br /&gt;"));

                if(upimg != '') {
                    if(postBlock.find(".ogp-thumb").length) postBlock.find(".ogp-thumb").html('&lt;img src="/img/' + upimg + '"&gt;');
                    else $('&lt;a href="" class="post-ogp post-movie" onclick="return false;"&gt;&lt;div class="ogp-thumb"&gt;&lt;img src="/img/' + upimg + '"&gt;&lt;/div&gt;&lt;/a&gt;').insertAfter(postBlock.find(".post-content"));
                }
            }
        }

        //????????
        $('#post-wrap').on('click', 'a[rel="recommendButton"]', function(){
            var button = $(this);
            var id = button.find('input[name="id"]').val();
            var kind = button.find('input[name="kind"]').val();
            $.ajax({
                url:"/likes/add",
                data:{'id':id, 'kind':kind},
                type:"POST",
                datatype:"html",
                success:function(data){
                    button.find('.post-count').html(data);
                },
                error:function(XMLHttpRequest, textStatus, errorThrown){}
            });
        });

        //?????????
        var pushFlag = 0;
        $('#btn_form_submit').click(function(){
            if (pushFlag) return false;
            var fd = new FormData($('form[id="post-form"]').get(0));
            $.ajax({
                type: "POST",
                url: "/posts/add",
                data:fd,
                dataType:"json",
                processData: false,
                contentType: false,
                success: function(data,dataType){
                    $('.error-message').empty();
                    if (typeof data == "string") {
                        $('.error-message').append(data);
                    }else{
                        var json = parseJsonSelect(data);
                        $("#post-wrap").prepend(json);
                        $(".post-box:first").hide();
                        $(".post-box:first").animate({height:'show',opacity:'show'});
                        $('form[id="post-form"]').find("textarea, :text, select").val("").end().find(":checked").prop("checked", false).end().find("option:selected").attr("selected", false).end().find(".select2-selection__choice").remove();
                        $('form[id="post-form"]').find("#range_0").prop("checked", true);
                        $('#btn_range').find('.tag-select').text('All Members');
                        $('#img').wrap('&lt;span id="span1"&gt;&lt;/span&gt;');
                        var area = document.getElementById('span1');
                        var temp = area.innerHTML;
                        area.innerHTML = temp;
                        $("#jurisdiction").select2({
                            placeholder: "Add Jurisdiction"
                        });
                        $("#lawField").select2({
                            placeholder: "Add Area of Law"
                        });
                        $("#postTags").select2({
                            placeholder: "Add Tags",
                        tags: true
                        });
                    }
					hidePost();
                },
                error: function(XMLHttpRequest, textStatus, errorThrown){}
            });
            pushFlag = 1;
            setTimeout(function(){ pushFlag = 0;}, 5000);
            return false;
        });

    });
</script>
<div id="modal_share" class="modal_win">
</div>

<script type="text/javascript">
var is_shared = false;
function getHtml(targetId, shareId, data)
{
    //var json = jurisdictionList;
    //var jsonLaw = lawFieldList;
    var juris =[];
    $.each(data.Admission, function(ky, val){
        juris.push(val);
    });
    var laws =[];
    $.each(data.SpecialField, function(ky, val){
        laws.push(val.SpecialField.law_field_prefix + val.SpecialField.law_field_id);
    });
    var tag =[];
    $.each(data.MyTag, function(ky, val){
        tag.push(ky);
    });
    var str  = '&lt;div style="overflow-y:scroll;overflow-x:hidden;max-height:100%;"&gt;'
             + '&lt;p class="txt_14 fw_b" style="background-color:#fff;"&gt;Share&lt;/p&gt;'
             + '&lt;form id="formShare" style="background-color:#fff;" onsubmit="doSomething();return false;"&gt;'
             + '&lt;input type="hidden" name="kind" value="'+data.kind+'"&gt;'
             + '&lt;input type="hidden" name="id" value="'+targetId+'"&gt;'
             + '&lt;input type="hidden" name="shareId" value="'+shareId+'"&gt;'
             + '&lt;input type="hidden" name="ctl" value="Mypage"&gt;'
             + '&lt;input type="hidden" name="act" value="index"&gt;'
             + '&lt;p class="txt_14 fw_b" id="display_err" style="background-color:#fff;"&gt;&lt;/p&gt;'
             +'&lt;div id="post-form_ms" style="background-color:#fff;"&gt;'
             +  '&lt;div class="clear" style="background-color:#fff;"&gt;'
             +   '&lt;div class="select-input-box" style="background-color:#fff;"&gt;'
             +     '&lt;select name="admission[]" id="jurisdiction_ms" multiple="multiple" style="background-color:#fff;"&gt;';
             var juri = "";
             $.each(data.jurisdiction_list, function(index, elem) {
                 juri += '&lt;option' + ((juris.indexOf(index) != -1) ? ' selected' : '') + ' value=' +index+ '&gt;' +elem+ '&lt;/option&gt;'
             });
             str += juri;
             str += ""
             +     '&lt;/select&gt;'
             +     '&lt;span class="btn_arrow" style="background-color:#fff;"&gt;&lt;/span&gt;'
             +   '&lt;/div&gt;'
             +   '&lt;div class="select-input-box" style="background-color:#fff;"&gt;'
             +     '&lt;select name="law_field[]" id="lawField_ms" multiple="multiple"&gt;';
             var law = "";
             $.each(data.law_field_list, function(index, elem) {
                 law += '&lt;option' + ((laws.indexOf(index) != -1) ? ' selected' : '') + ' value=' +index+ '&gt;' +elem+ '&lt;/option&gt;'
             });
             str += law;
             str += ""
             +     '&lt;/select&gt;'
             +     '&lt;span class="btn_arrow" style="background-color:#fff;"&gt;&lt;/span&gt;'
             +    '&lt;/div&gt;'
             +  '&lt;/div&gt;'
             +  '&lt;div class="post-txt textarea" style="background-color:#fff;"&gt;'
             +    '&lt;div class="spacer-scrollY" style="background-color:#fff;"&gt;'
             +      '&lt;textarea name="comment" placeholder="Text" wrap="hard" style="background-color:#fff;"&gt;&lt;/textarea&gt;'
             +    '&lt;/div&gt;'
             +    '&lt;div class="share-art" style="background-color:#fff;"&gt;'
			 +       '&lt;img src="' + data.User.image + '" width="36" height="36" style="border-radius:20px; float:left; margin-right:10px" /&gt;'
			 +       '&lt;p style="margin-bottom:0px;"&gt;&lt;span class="txt_13"&gt;&lt;b&gt;' + data.User.name + '&lt;/b&gt;&lt;/span&gt;&lt;/p&gt;'
             +       '&lt;p style="margin-bottom:15px"&gt;&lt;span class="txt_gray txt_11"&gt;' +data.created+ ' ? &lt;/span&gt;&lt;span class="fw_b"&gt;' +data.name+ '&lt;/span&gt;&lt;/p&gt;'
			 +       '';
             for (var Mo in data.Model) {break;}
             var detail = "";
             if (Mo == 'Post') {
                detail +=       '&lt;p class="txt_13" style="background-color:#fff;"&gt;' +data.Model.Post.comment+ '&lt;/p&gt;'
             }else if (Mo == 'Link') {
                detail +=       '&lt;p class="txt_13" style="background-color:#fff;"&gt;' +data.Model[Mo].title+ '&lt;/p&gt;'
                        +       '&lt;p class="txt_11" style="background-color:#fff;"&gt;' +data.Model[Mo].description+ '&lt;/p&gt;'
             }else {
                detail +=       '&lt;p class="txt_13" style="background-color:#fff;"&gt;' +data.Model[Mo].name+ '&lt;/p&gt;'
                        +       '&lt;p class="txt_11" style="background-color:#fff;"&gt;' +data.Model[Mo].detail+ '&lt;/p&gt;'
             }
             str += detail;
             str += ""
             +     '&lt;/div&gt;'
             +   '&lt;/div&gt;'
             +  '&lt;div class="clear bdt" style="background-color:#fff;"&gt;'
             +      '&lt;div id="btn_range_ms" class="select-input-box txt_11 fl" style="background-color:#fff;"&gt;'
             +          '&lt;span class="btn_arrow" style="background-color:#fff;"&gt;&lt;/span&gt;'
             +          '&lt;span class="btn_select txt_gray txt_11" style="background-color:#fff;"&gt;'
             +              '&lt;ul class="clear" style="background-color:#fff;"&gt;'
             +                  '&lt;li class="tag-select"&gt;All Members&lt;/li&gt;'
             +                  '&lt;li class="txt_range"&gt;&lt;div&gt;Who can see this post?&lt;/div&gt;&lt;/li&gt;'
             +              '&lt;/ul&gt;'
             +          '&lt;/span&gt;'
             +          '&lt;div class="chk-box" id="range" style="background-color:#fff;"&gt;'
             +             '&lt;input type="radio" value="1" name="range" id="range_0_ms" class="input_radio" checked&gt;&lt;label for="range_0_ms"&gt;All Members&lt;/label&gt;'
             +              '&lt;input name="range" value="3" type="radio" id="range_1_ms" class="input_radio"&gt;&lt;label for="range_1_ms"&gt;Limit those who can see&lt;/label&gt;'
             +              '&lt;div class="chk-box" id="range_det"&gt;'
             +                  '&lt;input type="checkbox" value="1" name="range_det[]" id="range_10_ms" class="input_checkbox"&gt;&lt;label for="range_10_ms"&gt;All attorneys&lt;/label&gt;'
             +                  '&lt;input type="checkbox" value="2" name="range_det[]" id="range_11_ms" class="input_checkbox"&gt;&lt;label for="range_11_ms"&gt;All clients&lt;/label&gt;'
             +                  '&lt;input type="checkbox" value="3" name="range_det[]" id="range_12_ms" class="input_checkbox"&gt;&lt;label for="range_12_ms"&gt;Attorneys connected&lt;/label&gt;'
             +                  '&lt;input type="checkbox" value="4" name="range_det[]" id="range_13_ms" class="input_checkbox"&gt;&lt;label for="range_13_ms"&gt;Clients connected&lt;/label&gt;'
             +               '&lt;/div&gt;'
             +             '&lt;/div&gt;'
             +          '&lt;/div&gt;'
             +      '&lt;/div&gt;'
             +   '&lt;/div&gt;'
             +   '&lt;div class="ta-c" style="background-color:#fff;"&gt;'
             +       '&lt;a class="modal_close"&gt;Cancel&lt;/a&gt;'
             +       '&lt;button class="modal_share_cload" type="button"&gt;Share&lt;/button&gt;'
             +   '&lt;/div&gt;'
             + '&lt;/form&gt;'
             + '&lt;script&gt;'
             + '$(function(){'
             +    '$("#jurisdiction_ms").select2({'
             +      'placeholder: "Add Jurisdiction"'
             +    '});'
             +    '$("#lawField_ms").select2({'
             +      'placeholder: "Add Area of Law"'
             +    '});'
             +    '$("#postTags_ms").select2({'
             +      'placeholder: "Add Tags",'
             +      'tags: true'
             +    '});'

             +    "$('#post-form_ms .select-input-box li:first-child input').css({'width':'229px','minHeight':'38px'});"
             +    "$('#post-form_ms .select-input-box .select2.select2-container.select2-container--default').css({'width':'229px','minHeight':'38px'});"
             +    "$('#bg_tag_ms li:first-child input').css({'width':'437px','minHeight':'38px'});"
             +    "$('#bg_tag_ms .bg_tag .select2.select2-container.select2-container--default').css({'width':'437px','minHeight':'38px'});"


             +    'var range = $( \'#btn_range_ms input[name="range"]\' );'
             +    "var range0 = $('#range_0_ms');"
             +    "var range1 = $('#range_1_ms');"
             +    "var range10 = $('#range_10_ms');"
             +    "var rangeX = $('#btn_range_ms input[type=checkbox]');"
             +    "var pushBox = $('#btn_range_ms ul');"

             +    "range.change( function() {"
             +        'if( this.id == "range_1_ms"){'
             +            "range10.prop('checked',true);"
             +            'pushBox.html(\'&lt;li class="tag-select"&gt;Limit those who can see&lt;/li&gt;\');'
             +        '}else{'
             +            "rangeX.prop('checked',false);"
             +            'pushBox.html(\'&lt;li class="tag-select"&gt;All Members&lt;/li&gt;\');'
             +        '}'
             +    '});'

             +    'rangeX.change( function(){'
             +        "if($('#btn_range_ms input[type=checkbox]:checked').length == 0){"
             +            "range0.prop('checked',true);"
             +            'pushBox.html(\'&lt;li class="tag-select"&gt;All Members&lt;/li&gt;\');'
             +        "}else{"
             +            "range1.prop('checked',true);"
             +            'pushBox.html(\'&lt;li class="tag-select"&gt;Limit those who can see&lt;/li&gt;\');'
             +        "}"
             +    "});"

             +    "var btnRange = $('#btn_range_ms');"
             +    "var chkBox = $('#btn_range_ms .chk-box');"
             +    "btnRange.click(function(evt){"
             +        "if(!btnRange.hasClass('open')){"
             +            "btnRange.addClass('open');"
             +        "}else if(!$(evt.target).closest(chkBox).length){"
             +            "btnRange.removeClass('open');"
             +        "}"
             +    "});"

             +    "$(document).on('click', function(evt){"
             +        "if( !$(evt.target).closest(btnRange).length ){"
             +            "btnRange.removeClass('open');"
             +        "}"
             +    "});"
             +  '});&lt;\/script&gt;'
             + '&lt;/div&gt;';
    return str;
}
$(function() {

    var body = $('body');
    var modalShare = $("#modal_share");

    $('#post-wrap').on('click',".modal_share_load",function(){
        var targetId=$(this).attr('targetId');
        var model=$(this).attr('model');
        var shareId=$(this).attr('shareId');
        //var jurisdictionList = "";
        //var lawFieldList = "";
        $.ajax({
            type: "POST",
            url: "/mypage/ajaxShareCheck",
            data: {model: model, id: targetId, shareId: shareId},
            dataType:"json",
            success: function(data,dataType){
                var str = getHtml(targetId, shareId, /*jurisdictionList, lawFieldList, */data);
                modalShare.html('');
                modalShare.html(str);
                body.addClass('modal-edit-active');
            },
            error: function(XMLHttpRequest, textStatus, errorThrown){}
        });
    });

    var pushFlag = 0;
    $(document).on('click',".modal_share_cload",function(){ // ???????????
        if (pushFlag) return false;
        var modalShareCURI = '/posts/share';
        var kind=$("#formShare [name=kind]").val();
        var id=$("#formShare [name=id]").val();
        var shareId=$("#formShare [name=shareId]").val();
        var act=$("#formShare [name=act]").val();
        var ctl=$("#formShare [name=ctl]").val();
        var uid=$("#formShare [name=uid]").val();
        var comment=$("#formShare [name=comment]").val();
        var admission = selMulti('jurisdiction_ms');
        var lawField = selMulti('lawField_ms');
        var range = selRadio('range');
        var range_det = checkMulti('range');
        var token = '098010f2b9655cc42010d9d6a7c7c99806d75d34';
		is_shared = true;
        $.ajax({
            type: "POST",
            url: modalShareCURI,
            data: {kind: kind, id: id, shareId: shareId, act: act, ctl: ctl, uid: uid, comment:comment, admission: admission, range: range, range_det: range_det, special_field: lawField, token: token},
            dataType:"json",
            success: function(data,dataType){

                if ( data == parseInt(data) ){
                    // ???
                    var str = "";
                    switch (data) {
                        case 1:
                          str = '&lt;p style="color:#FF0000;"&gt;Please fill out within 5 of items.&lt;/p&gt;';
                          break;
                        case 2:
                          str = '&lt;p style="color:#FF0000;"&gt;Please complete this field.&lt;/p&gt;';
                          break;
                    }

                    $("#display_err").html(str);

                } else {
                    // ????
                    $("#modal_share").hide();
                    $("a[rel*=share_comp]").leanModal({
                        top: 60,
                        overlay : 0.9,
                        closeButton: ".modal_close"
                    }).trigger('click');
					share_refresh();
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown){}
        });
        pushFlag = 1;
        setTimeout(function(){ pushFlag = 0;}, 3000);
    });
});
</script>

<a rel="share_comp" href="#share_comp"></a>
<div id="share_comp" class="modal_win">
      <p>Share</p>
      <p>This information has been posted successfully to the timeline</p>
      <a href="#modal_delete" class="modal_close">OK</a>
</div>

<script type="text/javascript">
	function share_refresh() {
		$("#share_comp .modal_close, div[id='lean_overlay']").click(function() {
			if(is_shared) {
				pos = $(window).scrollTop();
				window.location = '/mypage?p' + Math.round(1000 * Math.random()) + '#' + pos;
				is_shared = false;
			}
		});
	}
	pos = window.location.hash.replace('#', '');
	if(pos != '' &amp;&amp; parseInt(pos) &gt; 0) {
		$(window).scrollTop(parseInt(pos));
	}
</script>

<div id="modal_save" class="modal_saved" style="bottom: 0;margin: auto;display: none;width: 620px;height: 270px;text-align: center;background: #fff;padding: 79px;box-sizing: border-box;transition:.2s;">
    <p style="background-color:#fff;font-weight: bold;font-size: 12px;line-height: 1em;margin-bottom: 24px;">Save</p>
    <p style="background-color:#fff;margin-bottom: 32px;font-size: 12px;line-height: 1em;">Would you like to save this item to your list?</p>
    <div class="clear" style="background-color:#fff;width: 245px;margin: 0 auto;">
        <a class="modal_del_load" id="saveButton" style="margin-right: 5px;background: #44d095;float: left;display: block;width: 120px;height: 30px;font-size: 12px;line-height: 30px;text-align: center;color: #fff;" button="save">Save</a>
        <a href="#modal_delete" class="modal_close" style="margin: 0;float: left;display: block;width: 120px;height: 30px;font-size: 12px;line-height: 30px;text-align: center;color: #fff;background: #bbb;" button="close">Cancel</a>
    </div>
    <div id="afterDisplayDelete"></div>
    <div class="inf"></div>
</div>

<script>
    //??????????
    var token = '098010f2b9655cc42010d9d6a7c7c99806d75d34';
    var pushFlag = 0;
    $("#saveButton").click(function(e){
        if (pushFlag) return false;
        var path = "/posts/saved";
        var post_id=$("#inf").attr('post_id');
        var kind=$("#inf").attr('kind');

        $.post(path , {kind : kind, post_id : post_id, token : token}, function(returnPath){
            $(".inf").empty();
            if (returnPath == 1) {
                $("a[rel*=save_add_comp]").leanModal({
                    top: 60,
                    overlay : 0.9,
                    closeButton: ".modal_close"
                }).trigger('click');
                $('#post-wrap').find('a[sv="sv"]').css('background-image', 'url(/img/common/btn_share-selected.png)');

            } else if (returnPath == 2) {
                $("a[rel*=save_add_false]").leanModal({
                    top: 60,
                    overlay : 0.9,
                    closeButton: ".modal_close"
                }).trigger('click');
            }
            $('#post-wrap').find('a[sv="sv"]').attr('sv', 'svcomp');
        });
        pushFlag = 1;
        setTimeout(function(){ pushFlag = 0;}, 3000);
    });

    $(document).on('click','a[rel*=leansave]', function(){
        $(this).attr('sv', 'sv');
        var post_id = $(this).find('input[name="post_id"]').val();
        var kind = $(this).find('input[name="kind"]').val();
        var userId = $(this).find('input[name="user_id"]').val();
        $(".inf").append('&lt;div id="inf" post_id="' +post_id+ '" kind="' +kind+ '"&gt;&lt;/div&gt;');

    }).on('click',function(evt){
        if( !$(evt.target).closest($("#modal_save")).length &amp;&amp; !$(evt.target).closest($("a[rel*=leansave]")).length ){
            //body.removeClass('win_02');
            if ( !$(evt.target.hash).length ) {
                $(".inf").empty();
                $('#post-wrap').find('a[sv="sv"]').attr('sv', 'sv1');
            }
        }
    }).on('click', '.modal_close', function(){
        if ($(this).attr("button") == "close") {
            $(".inf").empty();
            $('#post-wrap').find('a[sv="sv"]').attr('sv', 'sv1');
        }
    });
</script>
<a rel="save_add_comp" href="#save_add_comp"></a>
<div id="save_add_comp" class="modal_win" style="bottom: 0;margin: auto;display: none;width: 620px;height: 270px;text-align: center;background: #fff;padding: 79px;box-sizing: border-box;transition:.2s;">
    <p>Save</p>
    <p>Your new item has been saved</p>
    <a href="#modal_delete" class="modal_close">OK</a>
</div>
<a rel="save_add_false" href="#save_add_false"></a>
<div id="save_add_comp" class="modal_win" style="bottom: 0;margin: auto;display: none;width: 620px;height: 270px;text-align: center;background: #fff;padding: 79px;box-sizing: border-box;transition:.2s;">
    <p>Save</p>
    <p>Failed</p>
    <a href="#modal_delete" class="modal_close">OK</a>
</div>
<a rel="edit_complete" href="#edit_complete"></a>
<div id="edit_complete" class="modal_win" style="bottom: 0;margin: auto;display: none;width: 620px;height: 270px;text-align: center;background: #fff;padding: 79px;box-sizing: border-box;transition:.2s;">
    <p>Save</p>
    <p>This information has been posted successfully to the timeline</p>
    <a href="#modal_delete" class="modal_close">OK</a>
</div>



<div id="win-btm">
    <p id="win-head" class="txt_13">Report bugs and suggestions</p>
    <div id="win-box">
            <textarea name="data[Feedback][contents]" placeholder="Text" id="feedbackText"></textarea>
            <input value="Send" class="bg_gray" id="feedback" type="submit">
            <p class="txt_13">
                <span>Thank you for your cooperation.</span><br>
            </p>
    </div>
</div>



<div id="line_footer">
<footer>
    <div id="footer">
        <a href="/">TOP</a>
        <a href="https://geo.itunes.apple.com/us/app/juristerra/id1108357440?mt=8" target="_blank">JURISTERRA APP</a>
                <a href="/faq">FAQ</a>
        <a href="/terms">TERMS OF SERVICE</a>
        <a href="/privacy">PRIVACY POLICY</a>
        <a href="/inquiries">CONTACT</a>
    </div>
    <div id="foot-cp">
        COPYRIGHT &copy; JURISTERRA ALL RIGHTS RESERVED.
    </div>
</footer>
<script type="text/javascript" src="/js/plugin/select2.min.js"></script>
<script src="/js/common/common.js"></script>
<script src="/js/plugin/jquery.leanModal.min.js"></script>
  <script src="/js/common/invite_modal.js"></script>
  <div id="modal_inv">
      <div class="modal-explain">
        <h2 class="txt_14 fw_b">Invite colleagues and acquintances to Juristerra</h2>
        <p class="txt-detail">
          Invite others either through an email on the left, or a standalone link on the right<br>
          You may invite a colleague to Juristerra        </p>
      </div>
      <div class="ta_c">
        <a href="/invites/add" class="btn_top">
          <span class="icon_share"><img src="/img/invite/icon_share_link.png" alt="Send an invitation email"></span>
          Send an invitation email        </a>

        <a href="/invites/addlink" class="btn_top_link btn_top_link">
          <span class="icon_share"><img src="/img/invite/icon_share_mail.png" alt="Send an invitation link"></span>
          Send an invitation link</a>
      </div>
  </div>
  <script>
    $(function(){
        $( 'a[rel*=shareModal]').leanModal({
            top: 0,                     // ????????????????
            overlay : 0.9,               // ??????
            closeButton: ".modal_close"  // ???????CSS class???
        });
        $( 'a[rel*=leanModal]').leanModal({
            top: 150,                     // ????????????????
            overlay : 0.9,               // ??????
            closeButton: ".modal_close"  // ???????CSS class???
        });
        $( 'a[rel*=searchModal]').leanModal({ top: 0, overlay : 0.9, closeButton: ".modal_close" });
        $( 'a[rel*=searchModal]').click(function() { $.ajax({ url: "/lawyerSearch/modal/" }).done(function(html) { $("#modal_search").html(html); }); });

        var modalInv = $('#modal_inv');
        var inviteStyle = 'display: block; left: 50%; margin-left: -340px; opacity: 1; position: fixed; top: 50%; transform: translateY(-50%); -webkit-transform: translateY(-50%); -moz-transform: translateY(-50%); z-index: 11000; max-height:98%; overflow-y:auto;';

        $(document).on('click',"#modalInvOpen",function(){
            modalInv.attr('style',inviteStyle).removeClass('complete').clearQueue();
        });
    });
  </script>

<script src="/js/common/login_modal.js"></script>
<script>
$(function(){
    $( 'a[rel*=leanModal]').leanModal({
        top: 20,                     // ????????????????
        overlay : 0.9,               // ??????
        closeButton: ".modal_close"  // ???????CSS class???
    });
});
</script>


<form action="/lawyerSearch/" method="post" id="modal_search"></form>


<script>
$(function(){
    $("#jurisdiction_s").select2({
      placeholder: "Text"
    });
    $("#lawField_s").select2({
      placeholder: "Text"
    });
    $("#businessArea_s").select2({
      placeholder: "Text"
    });
    $(".postTags").select2({
      placeholder: "Tags",
      tags: true
    });
});
</script>


<script src="/js/plugin/jquery.slimscroll.min.js"></script>
<script>
$(function(){
    $("#jurisdiction").select2({
      placeholder: "Add Jurisdiction"
    });
    $("#lawField").select2({
      placeholder: "Add Area of Law"
    });
    $("#postTags").select2({
      placeholder: "Add Tags",
      tags: true
    });
    $("#sortby").select2({
      placeholder: "Sort By"
    });
    $("#feedback").click(function(e){
        var path = "/feedbacks/";
        var value=$("#feedbackText").val();
        $.post(path , {feedbackText : value}, function(data){
            $("#afterDisplay").append(data);
        })
    });

	if(window.location.hash == '#login_err') $("#invite_modalOpen2").trigger('click');
	if("1" == 0) $("#invite_modalOpen2").trigger('click');
});
</script>



</div><div id="lean_overlay"></div><div id="lean_overlay"></div><div id="lean_overlay"></div><div id="lean_overlay"></div><div id="lean_overlay"></div><div id="lean_overlay"></div><div id="lean_overlay"></div><script style="display: none;" aria-hidden="true" type="application/x-lastpass" id="hiddenlpsubmitdiv"></script><script>try{(function() { for(var lastpass_iter=0; lastpass_iter &lt; document.forms.length; lastpass_iter++){ var lastpass_f = document.forms[lastpass_iter]; if(typeof(lastpass_f.lpsubmitorig2)=="undefined"){ lastpass_f.lpsubmitorig2 = lastpass_f.submit; if (typeof(lastpass_f.lpsubmitorig2)=='object'){ continue;}lastpass_f.submit = function(){ var form=this; var customEvent = document.createEvent("Event"); customEvent.initEvent("lpCustomEvent", true, true); var d = document.getElementById("hiddenlpsubmitdiv"); if (d) {for(var i = 0; i &lt; document.forms.length; i++){ if(document.forms[i]==form){ if (typeof(d.innerText) != 'undefined') { d.innerText=i.toString(); } else { d.textContent=i.toString(); } } } d.dispatchEvent(customEvent); }form.lpsubmitorig2(); } } }})()}catch(e){}</script></body></html>